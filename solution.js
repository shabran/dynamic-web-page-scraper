const fs = require('fs');
const rp = require('request-promise');
const $ = require('cheerio');
const url = 'https://www.bankmega.com/promolainnya.php';
const parser = require('./parser');
const scrape = require('./scraper');

function run(cat) {
    let Urls = [];
    // console.log(Urls)
    return scrape(cat).then(data => {
        Urls = data;
    }).then(() => {
        return Promise.all(
            Urls.map(function(data) {
                return parser('https://www.bankmega.com/' + data.url);
            })
        ).then(function(data) {
            let jason = {
                [cat]: data
            };
            return jason;
        })
    })

}

function getCat(url) {
    return rp(url)
        .then(function(html) {
            //success!
            const cat = [];
            for (let i = 0; i < 6; i++) {
                cat.push($('#subcatpromo > div > img', html)[i].attribs.id);
            }
            return cat;
        })
        .catch(function(err) {
            //handle error
        });
}

getCat(url).then(data => {
    let promise1 = run(data[0]);
    let promise2 = run(data[1]);
    let promise3 = run(data[2]);
    let promise4 = run(data[3]);
    let promise5 = run(data[4]);
    let promise6 = run(data[5]);

    Promise.all([promise1, promise2, promise3, promise4, promise5, promise6]).then((values) => {
        let output = JSON.stringify(values);
        fs.writeFile("solution.json", output, 'utf8', (err) => {
            if (err) {
                console.log("An error occured while writing JSON Object to File.");
                return console.log(err);
            }
            console.log("JSON file has been saved.");
        });
    })
});