const puppeteer = require('puppeteer');
const url = 'https://www.bankmega.com/promolainnya.php';

function scrape(cat) {
    return new Promise(async(resolve, reject) => {
        try {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            await page.goto(url);
            await page.click(`#${cat}`);
            await page.waitFor(1000);

            const pagesToScrape = await page.evaluate(() => {
                let item = document.querySelector('#paging1[title]');
                let a;
                try {
                    a = item.getAttribute('title');
                } catch (error) {
                    a = null;
                }
                if (!a) {
                    return null
                }
                let sliced = a.slice(9);
                return parseInt(sliced);
            })
            let currentPage = 1;
            let urls = [];
            while (currentPage <= pagesToScrape) {
                await page.waitForSelector('#promolain > li > a[href]');
                let newUrls = await page.evaluate(() => {
                    let results = [];
                    let items = document.querySelectorAll('#promolain > li > a[href]');
                    items.forEach((item) => {
                        results.push({
                            url: item.getAttribute('href')
                        });
                    });
                    return results;
                });
                urls = urls.concat(newUrls);
                if (currentPage < pagesToScrape) {
                    await Promise.all([
                        // await page.waitFor(100),
                        // await page.waitForSelector('#contentpromolain2 > div:nth-child(2) > div > table > tbody > tr > td:nth-child(12) > a'),
                        await page.click(`a.page_promo_lain[page="${currentPage+1}"]`),
                        await page.waitFor(200),
                        await page.waitForSelector('#promolain > li > a[href]')
                    ])
                }
                currentPage++;
            }
            browser.close();
            return resolve(urls);
        } catch (err) {
            return reject(err);
        }
    })
}

// scrape("gadget_entertainment").then(console.log).catch(console.error);

module.exports = scrape;