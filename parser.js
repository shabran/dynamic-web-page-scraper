const rp = require('request-promise');
const $ = require('cheerio');

const parser = (url) => {
    return rp(url)
        .then((html) => {
            return {
                title: $('#contentpromolain2 > div.titleinside > h3', html).text(),
                imageurl: $('img', '#contentpromolain2 > div.keteranganinside', html).attr('src'),
                area_promo: $('#contentpromolain2 > div.area > b', html).text(),
                periode_promo: $('#contentpromolain2 > div.periode > b', html).text()
            };
        })
        .catch((err) => {
            console.log(err);
            //handle error
        });
};

module.exports = parser;